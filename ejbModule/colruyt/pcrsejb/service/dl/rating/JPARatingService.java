package colruyt.pcrsejb.service.dl.rating;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import colruyt.pcrsejb.entity.competence.DomainCompetence;
import colruyt.pcrsejb.entity.competence.OperatingUnitCompetence;
import colruyt.pcrsejb.entity.operatingunit.OperatingUnit;
import colruyt.pcrsejb.entity.survey.Rating;

public class JPARatingService implements RatingService {

	@PersistenceContext(name = "Traineeship2018EJB")
	EntityManager em;
	
	@Override
	public Rating save(Rating element) {
		return em.merge(element);
	}

	@Override
	public Rating getElement(Rating element) {
		return em.find(Rating.class, element.getId());
	}

	@Override
	public Collection<Rating> getAllElements() {
		TypedQuery<Rating> query = em.createQuery("select r from rating r", Rating.class);
		List<Rating> listOfRatings = query.getResultList();
		return listOfRatings;
	}

	@Override
	public void deleteElement(Rating element) {
		em.remove(element);
	}

	@Override
	public List<DomainCompetence> getAllDomainCompetences() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<OperatingUnitCompetence> getAllOperatingUnitCompetencesFor(OperatingUnit unit) {
		// TODO Auto-generated method stub
		return null;
	}

}
