package colruyt.pcrsejb.service.dl.enrolment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;

import colruyt.pcrsejb.entity.enrolment.Enrolment;
import colruyt.pcrsejb.entity.role.Role;
import colruyt.pcrsejb.entity.team.Team;
import colruyt.pcrsejb.entity.userPrivilege.UserPrivilege;
import colruyt.pcrsejb.service.dl.DbService;
import colruyt.pcrsejb.service.dl.role.DbRoleService;
import colruyt.pcrsejb.service.dl.role.RoleService;
import colruyt.pcrsejb.service.dl.user.DbUserService;
import colruyt.pcrsejb.service.dl.user.UserService;
import colruyt.pcrsejb.service.dl.userPrivilege.DbUserPrivilegeService;
import colruyt.pcrsejb.service.dl.userPrivilege.UserPrivilegeService;

@Stateless
public class DbEnrolmentService extends DbService implements EnrolmentService {
	
	private static final long serialVersionUID = 1L;
	private UserService userService = new DbUserService();
	private RoleService roleService = new DbRoleService();
	private UserPrivilegeService userPrivilegeService = new DbUserPrivilegeService();
	
	private static final String GET_ENROLMENTS_FROM_TEAM = "Select * from teamenrolments where team_id = ?";
	private static final String GET_ENROLMENTS_FROM_USERPRIVILEGE = "SELECT * FROM teamenrolments where userprivileges_ID = ?";

	@Override
	public Enrolment save(Enrolment element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enrolment getElement(Enrolment element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Enrolment> getAllElements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteElement(Enrolment element) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Set<Enrolment> getEnrolmentsForTeam(Team team) {
		Set<Enrolment> enrolments = new HashSet<>();
		try (Connection conn = this.createConnection()) {
			PreparedStatement preparedStatement = conn.prepareStatement(GET_ENROLMENTS_FROM_TEAM);
			preparedStatement.setInt(1, team.getId());
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				Enrolment enrolment = new Enrolment();
				enrolment.setActive("1".equalsIgnoreCase(rs.getString("ACTIVE")));
				enrolment.setId(rs.getInt("Id"));
				enrolment.setPrivilege(userPrivilegeService.getElement(new UserPrivilege(rs.getInt("Userprivileges_id"))));
				enrolment.setUser(userPrivilegeService.getUserfromUserPrivileges(enrolment.getPrivilege()));
				if (rs.getInt("ROLES_ID") != 0) {
					enrolment.setRole(roleService.getElement(new Role(rs.getInt("ROLES_ID"))));
				}

				enrolments.add(enrolment);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return enrolments;
	}

	@Override
	public List<Enrolment> getEnrolmentsForPrivilege(UserPrivilege privilege) {
		List<Enrolment> enrolments = new ArrayList<>();
		try (Connection conn = this.createConnection()) {
			PreparedStatement preparedStatement = conn.prepareStatement(GET_ENROLMENTS_FROM_USERPRIVILEGE);
			preparedStatement.setInt(1, privilege.getId());
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				Enrolment enrolment = new Enrolment();
				enrolment.setActive("1".equalsIgnoreCase(rs.getString("ACTIVE")));
				enrolment.setId(rs.getInt("Id"));
				enrolment.setPrivilege(userPrivilegeService.getElement(new UserPrivilege(rs.getInt("Userprivileges_id"))));
				enrolment.setUser(userPrivilegeService.getUserfromUserPrivileges(enrolment.getPrivilege()));
				enrolments.add(enrolment);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return enrolments;
	}
}