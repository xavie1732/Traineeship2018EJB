package colruyt.pcrsejb.service.dl.user;

import java.util.List;
import java.util.Map;

import colruyt.pcrsejb.entity.user.User;
import colruyt.pcrsejb.entity.userPrivilege.FunctionResponsibleUserPrivilege;
import colruyt.pcrsejb.entity.userPrivilege.UserPrivilege;
import colruyt.pcrsejb.service.dl.GenericCrudService;

public interface UserService extends GenericCrudService<User,Integer>{
    List<User> findUsersByFirstName(String name);
    List<User> findUsersByShortName(String shortName);
    User getFunctionResponsible(int functionsId, String country);
    List<User> getAllFunctionResponsibles();

	User getElementByEmail(String email);

    Map<FunctionResponsibleUserPrivilege, User> getFunctionResponsibles();

    List<UserPrivilege> findPrivilegesForUser(User user);
}