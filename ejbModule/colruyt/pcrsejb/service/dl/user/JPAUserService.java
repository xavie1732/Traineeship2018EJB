package colruyt.pcrsejb.service.dl.user;

import java.io.Serializable;
import java.util.Collection;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.entity.user.User;
import colruyt.pcrsejb.entity.userPrivilege.FunctionResponsibleUserPrivilege;
import colruyt.pcrsejb.entity.userPrivilege.PrivilegeType;
import colruyt.pcrsejb.entity.userPrivilege.UserPrivilege;
import colruyt.pcrsejb.service.dl.DbService;

@Stateless
@LocalBean
public class JPAUserService extends DbService implements UserService, Serializable {

	private static final long serialVersionUID = 1L;
	
	
	@PersistenceContext(unitName = "Traineeship2018EJB")
	private EntityManager em;
	
	public JPAUserService() {
		super();
	}

	@Override
	public User save(User element) {
		System.out.println("testing");
		em.persist(element);
//		User user = em.merge(element);
//		if (user != null) {
//			return user;
//		}
//		else {
//			throw new EmptyStackException();
//		}
		return element;
	}

	@Override
	public User getElement(User element) {
		User user = em.find(User.class, element);
		if (user != null) {
			return user;
		} 
		else {
			throw new EmptyStackException();
		}
	}

	@Override
	public Collection<User> getAllElements() {
		TypedQuery<User> q = em.createQuery("SELECT u from User u", User.class);
		List<User> userList = q.getResultList();
		return userList;
	}

	@Override
	public void deleteElement(User element) {
		User user = em.find(User.class, element);
		if (user != null) {
			em.remove(element);
		}
		else {
			throw new EmptyStackException();
		}
	}

	@Override
	public List<User> findUsersByFirstName(String name) {
		Query q = em.createQuery("select u from User u where u.firstName like :fn");
		q.setParameter("fn", "%"+name+"%");
		return q.getResultList();
	}

	@Override
	public List<User> findUsersByShortName(String shortName) {
		Query q = em.createQuery("Select u from User u where upper(u.firstName) like upper(:fn) and upper(u.lastName) like upper(:ln)");

		String firstname = shortName.substring(0,2).toUpperCase();
		String lastname = shortName.substring(2).toUpperCase();
		q.setParameter("fn", firstname+"%");
		q.setParameter("ln", lastname+"%");

		return q.getResultList();
	}

	@Override
	public User getFunctionResponsible(int functionsId, String country) {
		Query qp = em.createQuery("SELECT u from User u, FunctionResponsibleUserPrivilege up where up.privilegeType = :privisType and up.function.id = :functionsId and up.country = :country and up.active = true");
		qp.setParameter("privisType", PrivilegeType.FUNCTIONRESPONSIBLE);
		qp.setParameter("functionsId", functionsId);
		qp.setParameter("country", country);

		return (User)qp.getSingleResult();
	}

	@Override
	public List<User> getAllFunctionResponsibles() {
		Query qp = em.createQuery("SELECT u from User u, UserPrivilege up where up.privilegeType = :privisType and up.active = true");
		qp.setParameter("privisType", PrivilegeType.FUNCTIONRESPONSIBLE);

		return qp.getResultList();
	}

	@Override
	public User getElementByEmail(String email) {
		Query q = em.createQuery("SELECT u from User u where UPPER(u.email) = UPPER(:email)");
		q.setParameter("email", email);
		return (User) q.getSingleResult();
	}

	@Override
	public Map<FunctionResponsibleUserPrivilege, User> getFunctionResponsibles() {
		//TODO

		/*Map<FunctionResponsibleUserPrivilege, User> functionResponsibleMap = new HashMap<>();
		User user = new User();
		Function function = new Function();

		FunctionResponsibleUserPrivilege up = null;

		try (Connection conn = this.createConnection()) {
			PreparedStatement statement = conn.prepareStatement(GET_FUNCTION_RESPONSIBLE_IDS);
			statement.setInt(1, PrivilegeType.FUNCTIONRESPONSIBLE.getId());
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				user.setId(rs.getInt("user_id"));
				function.setId(rs.getInt("functions_id"));
				user = getElement(user);
				function = fs.getElement(function);
				up = new FunctionResponsibleUserPrivilege(PrivilegeType.FUNCTIONRESPONSIBLE, true, function, rs.getString("country"));
				functionResponsibleMap.put(up, user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return functionResponsibleMap;*/
		return null;
	}

	@Override
	public List<UserPrivilege> findPrivilegesForUser(User user) {
		return null;
	}

	public Function getFunctionForPerson(User user) {
		// TODO Auto-generated method stub
		return null;
	}
}