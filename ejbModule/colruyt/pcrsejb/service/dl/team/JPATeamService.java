package colruyt.pcrsejb.service.dl.team;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import colruyt.pcrsejb.entity.enrolment.Enrolment;
import colruyt.pcrsejb.entity.team.Team;
import colruyt.pcrsejb.entity.user.User;
import colruyt.pcrsejb.service.dl.DbService;

@Stateless
public class JPATeamService extends DbService implements TeamService {

	private static final long serialVersionUID = 1L;
	@PersistenceContext(name = "Traineeship2018EJB")
	EntityManager em;

	@Override
	public Team save(Team element) {
		em.persist(element);
		em.flush();
		return element;
	}

	@Override
	public Team getElement(Team element) {
		return em.find(Team.class, element.getId());
	}

	@Override
	public Collection<Team> getAllElements() {
		TypedQuery<Team> query = em.createNamedQuery("Team.getAllElements", Team.class);
		return query.getResultList();
	}

	@Override
	public void deleteElement(Team element) {
		em.remove(element);
	}

	@Override
	public Team findTeamOfUser(User user) {
		TypedQuery<Team> query = em.createNamedQuery("Team.getTeamOfUser", Team.class);
		query.setParameter(":id", user.getId());
		return query.getSingleResult();
	}

	@Override
	public Team findTeamOfEnrolments(Enrolment e) {
		TypedQuery<Team> query = em.createNamedQuery("Team.getTeamOfEnrolment", Team.class);
		query.setParameter(":id", e.getId());
		return query.getSingleResult();
	}

	public Team getTeam(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	public User getOwnerOfTeam(Team team) {
		// TODO Auto-generated method stub
		return null;
	}
}
