package colruyt.pcrsejb.service.dl.role;

import java.util.Set;

import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.entity.role.Role;
import colruyt.pcrsejb.service.dl.GenericCrudService;

public interface RoleService extends GenericCrudService<Role,Integer>{
    Set<Role> getAllRolesForFunction(Function function);
}