package colruyt.pcrsejb.service.dl.role;

import java.io.Serializable;
import java.util.Collection;
import java.util.EmptyStackException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.entity.role.Role;
import colruyt.pcrsejb.service.dl.DbService;

@Stateless
@LocalBean
public class JPARoleService extends DbService implements RoleService, Serializable{

	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(name = "Traineeship2018EJB")
	EntityManager em;
	
	public JPARoleService() {
		super();
	}
	
	@Override
	public Role save(Role element) {
		Role role = em.merge(element);
		if (role != null) {
			return role;
		} 
		else {
			throw new EmptyStackException();
		}
	}

	@Override
	public Role getElement(Role element) {
		Role role = em.find(Role.class, element);
		if (role != null) {
			return role;
		} 
		else {
			throw new EmptyStackException();
		}
	}

	@Override
	public Collection<Role> getAllElements() {
		//TODO
		TypedQuery<Role> q = em.createQuery("select r from Role r", Role.class);
		List<Role> roleList =  q.getResultList();
		return roleList;
	}

	@Override
	public void deleteElement(Role element) {
		Role role = em.find(Role.class, element);
		if (role != null) {
			em.remove(element);
		}
		else {
			throw new EmptyStackException();
		}
	}

	@Override
	public Set<Role> getAllRolesForFunction(Function function) {
		//TODO check dit (tabel functionroles in query)!
		TypedQuery<Role> query = em.createQuery("select r from Role r where r.function in (select f from Function f where f.id = := function_id)", Role.class);
		query.setParameter("function_id", function.getId());
		List<Role> listOfRoles = (List<Role>) query.getResultList();
		Set<Role> setOfRoles = new HashSet<>(listOfRoles);
		return setOfRoles;
	}

}
