package colruyt.pcrsejb.service.dl;

import colruyt.pcrsejb.util.factories.ConnectionFactory;
import colruyt.pcrsejb.util.factories.ConnectionType;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import javax.ejb.Stateless;
@Stateless
public class DbService implements Serializable {

	private static final long serialVersionUID = 1L;

	public Connection createConnection() throws SQLException {
        return ConnectionFactory.createFactory(ConnectionType.BASIC).createConnection();
    }
}
