package colruyt.pcrsejb.service.dl.function;

import java.util.List;

import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.service.dl.GenericCrudService;


public interface FunctionService extends GenericCrudService<Function,Integer> {

    List<Function> getAllFunctionNames();

}
