package colruyt.pcrsejb.service.dl.function;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.entity.operatingunit.OperatingUnit;

@Stateless
@LocalBean
public class JPAFunctionService implements FunctionService {

    @PersistenceContext(unitName = "Traineeship2018EJB")
    EntityManager em;

    @Override
    public List<Function> getAllFunctionNames() {
        List<Function> functionList = new ArrayList<>();

        Function function = new Function();
        OperatingUnit op = new OperatingUnit();

        String query =  "SELECT f.id, f.title, f.operatingUnit.operatingUnitName FROM Function f";
        for (Iterator it = em.createQuery(query).getResultList().iterator(); it.hasNext();) {
            Object[] tuple = (Object[]) it.next();
            function.setId((Integer) tuple[0]);
            function.setTitle((String) tuple[1]);
            op.setOperatingUnitName((String) tuple[2]);
            function.setOperatingUnit(op);

        }
        return functionList;
    }

    @Override
    public Function save(Function element) {
        return em.merge(element);
    }

    @Override
    public Function getElement(Function element) {
        System.out.println(element.getId());
        return em.find(Function.class, element.getId());
    }

    @Override
    public Collection<Function> getAllElements() {
        TypedQuery<Function> query = em.createNamedQuery("Function.getAllFunctions", Function.class);
        return query.getResultList();
    }

    @Override
    public void deleteElement(Function element) {
        em.remove(element);
    }
}
