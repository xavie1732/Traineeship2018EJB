package colruyt.pcrsejb.service.dl.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.EmptyStackException;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import colruyt.pcrsejb.entity.domain.Domain;
import colruyt.pcrsejb.service.dl.DbService;

@Stateless
@LocalBean
public class JPADomainService extends DbService implements DomainService, Serializable {

	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(unitName = "Traineeship2018EJB")
	private EntityManager em;
	
	public JPADomainService() {
		super();
	}

	@Override
	public Domain save(Domain element) {
		Domain domain = em.merge(element);
		if (domain != null) {
			return domain;
		} 
		else {
			throw new EmptyStackException();
		}
	}

	@Override
	public Domain getElement(Domain element) {
		Domain domain = em.find(Domain.class, element);
		if (domain != null) {
			return domain;
		} 
		else {
			throw new EmptyStackException();
		}
	}

	@Override
	public Collection<Domain> getAllElements() {
		//TODO
		TypedQuery<Domain> q = em.createQuery("SELECT c from Competences c where c.name = DOMAIN", Domain.class);
		List<Domain> domainList = q.getResultList();
		return domainList;
	}

	@Override
	public void deleteElement(Domain element) {
		Domain domain = em.find(Domain.class, element);
		if (domain != null) {
			em.remove(element);
		}
		else {
			throw new EmptyStackException();
		}
	}

	@Override
	public List<Domain> findDomainsByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}
}