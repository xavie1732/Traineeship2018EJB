package colruyt.pcrsejb.service.dl.domain;

import java.util.List;

import colruyt.pcrsejb.entity.domain.Domain;
import colruyt.pcrsejb.service.dl.GenericCrudService;

public interface DomainService extends GenericCrudService<Domain,Integer>{

    List<Domain> findDomainsByName(String name);
}