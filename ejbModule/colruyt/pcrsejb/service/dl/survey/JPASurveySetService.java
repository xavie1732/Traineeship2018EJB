package colruyt.pcrsejb.service.dl.survey;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import colruyt.pcrsejb.entity.survey.Survey;
import colruyt.pcrsejb.entity.survey.SurveySet;
import colruyt.pcrsejb.entity.user.User;
import colruyt.pcrsejb.entity.userPrivilege.PrivilegeType;
import colruyt.pcrsejb.entity.userPrivilege.TeamMemberUserPrivilege;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@Stateless
public class JPASurveySetService implements SurveySetService {


    @PersistenceContext(name = "Traineeship2018EJB")
    EntityManager em;

    @Override
    public SurveySet save(SurveySet element) {


        return em.merge(element);



    }

    @Override
    public SurveySet getElement(SurveySet element) {

        return em.find(SurveySet.class, element.getId());
    }

    @Override
    public Collection<SurveySet> getAllElements() {

        return (List<SurveySet>) em.createQuery("select ss from SurveySet ss").getResultList();

    }

    @Override
    public void deleteElement(SurveySet element) {
        em.remove(element);
        em.flush();
    }

    @Override
    public List<SurveySet> findSurveySetsByUser(User u) {

        //TODO: Maak slechter (lambda's weg!!)

        TeamMemberUserPrivilege privi= (TeamMemberUserPrivilege) em.find(User.class, u.getId()).getPrivileges().stream()
                .filter(x-> x.getPrivilegeType().equals(PrivilegeType.TEAMMEMBER) && x.isActive())
                .findFirst().get();
        return new ArrayList<SurveySet>(privi.getSurveySetTreeSet());



    }

    @Override
    public SurveySet findSurveySetsByUserAndDate(User u, LocalDate year) {


        //TODO: Maak slechter (lambda's weg!!)

        TeamMemberUserPrivilege privi = (TeamMemberUserPrivilege) em.find(User.class, u.getId()).getPrivileges().stream()
                .filter(x-> x.getPrivilegeType().equals(PrivilegeType.TEAMMEMBER) && x.isActive())
                .findFirst().get();

        Optional<SurveySet> opt = privi.getSurveySetTreeSet().stream()
                .filter(x->x.getSurveyYear().getYear() == year.getYear())
                .findFirst();

        if(opt.isPresent()) {
            em.detach(opt.get());
            return opt.get();
        }
        else
        {
            return null;
        }


    }

    @Override
    public User findUserBySurvey(Survey survey) {

        //TODO: Geen idee hoe , zonder alle users af te gaan ???
        throw new NotImplementedException();
    }

    @Override
    public SurveySet findLastSurveySetForUser(User user) {


        //TODO: Maak slechter (lambda's weg!!)

        TeamMemberUserPrivilege privi= (TeamMemberUserPrivilege) em.find(User.class, user.getId()).getPrivileges().stream()
                .filter(x-> x.getPrivilegeType().equals(PrivilegeType.TEAMMEMBER) && x.isActive())
                .findFirst().get();

        em.detach(((TreeSet<SurveySet>) privi.getSurveySetTreeSet()).first());
        return ((TreeSet<SurveySet>) privi.getSurveySetTreeSet()).first();




    }



}
