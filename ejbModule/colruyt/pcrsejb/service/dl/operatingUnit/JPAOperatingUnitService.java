package colruyt.pcrsejb.service.dl.operatingUnit;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import colruyt.pcrsejb.entity.operatingunit.OperatingUnit;

public class JPAOperatingUnitService implements OperatingUnitService {

	@PersistenceContext(name = "Traineeship2018EJB")
	EntityManager em;
	
	@Override
	public OperatingUnit save(OperatingUnit element) {
		return em.merge(element);
	}

	@Override
	public OperatingUnit getElement(OperatingUnit element) {
		return em.find(OperatingUnit.class, element.getId());

	}

	@Override
	public Collection<OperatingUnit> getAllElements() {
		Query query = em.createQuery("select o from OperatingUnit o");
		List<OperatingUnit> listOfOperatingUnits = (List<OperatingUnit>) query.getResultList();
		return listOfOperatingUnits;
	}

	@Override
	public void deleteElement(OperatingUnit element) {
		em.remove(element);
	}

}
