package colruyt.pcrsejb.service.dl.competence;

import java.util.Collection;

import colruyt.pcrsejb.entity.competence.Competence;
import colruyt.pcrsejb.service.dl.GenericCrudService;



public interface CompetenceService extends GenericCrudService<Competence,Integer> {
	
	Collection<Competence> findAllFunctionCompetences();
	Collection<Competence> findAllCraftCompetences();

}
