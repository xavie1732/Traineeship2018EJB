package colruyt.pcrsejb.service.dl.competence;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import colruyt.pcrsejb.entity.competence.Competence;
import colruyt.pcrsejb.service.dl.DbService;
@Stateless
public class JPACompetenceService extends DbService implements CompetenceService{

	private static final long serialVersionUID = 1L;

	@PersistenceContext(name = "Traineeship2018EJB")
	EntityManager em;
	 
	CompetenceService cs = new DbCompetenceService();
		
		@Override
		public Competence save(Competence element) {
			em.persist(element);
			return cs.save(element);
		}
		
		@Override
		public Competence getElement(Competence element) {
			return cs.getElement(element);
		}
		
		@Override
		public Collection<Competence> getAllElements() {
			return cs.getAllElements();
		}
		
		@Override
		public void deleteElement(Competence element) {
			cs.deleteElement(element);
		}
		
		@Override
		public Collection<Competence> findAllFunctionCompetences() {
			return cs.findAllFunctionCompetences();
		}
		
		@Override
		public Collection<Competence> findAllCraftCompetences() {
			return cs.findAllCraftCompetences();
		}
}
