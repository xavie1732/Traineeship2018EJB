package colruyt.pcrsejb.service.bl;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.entity.enrolment.Enrolment;
import colruyt.pcrsejb.service.dl.enrolment.EnrolmentService;

@Stateless
public class EnrolmentBL implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
    EnrolmentService enrolmentDb;

    public Enrolment save(Enrolment enrolment) {
        return enrolmentDb.save(enrolment);
    }
}
