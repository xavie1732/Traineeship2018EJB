package colruyt.pcrsejb.service.bl;

import java.util.Collection;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.entity.role.Role;
import colruyt.pcrsejb.service.dl.role.JPARoleService;


@Stateless
public class RoleServiceBL {

    @EJB
	private JPARoleService roledb;
	
	// Altijd op Abstract werken.
	//private DomainService domaindb = new JPADomainService();

    public Set<Role> getAllRolesForFunction(Function function) {
        return roledb.getAllRolesForFunction(function);
    }

	public Role saveRole(Role role){
		return roledb.save(role);
	}

	public Collection<Role> getAllRoles() {
		return roledb.getAllElements();
	}

	public void delete(Role role) {
		roledb.deleteElement(role);
	}

	public Role getRole(Role role) {
		return roledb.getElement(role);
	}
}
