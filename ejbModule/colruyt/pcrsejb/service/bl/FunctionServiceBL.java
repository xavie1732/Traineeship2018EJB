
package colruyt.pcrsejb.service.bl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.service.dl.function.JPAFunctionService;
import colruyt.pcrsejb.util.exceptions.validation.ValidationException;
import colruyt.pcrsejb.util.validators.function.FunctionValidator;

@Stateless
public class FunctionServiceBL {

	@EJB
	private JPAFunctionService functionService;
    //private FunctionService functionService = new DbFunctionService();
    private FunctionValidator functionValidator = new FunctionValidator();

    public List<Function> getAllFunctionNames() {
        return (List<Function>) functionService.getAllFunctionNames();
    }


    public Function getFunction(Function function) {
        return functionService.getElement(function);
    }


	public Function saveFunction(Function function) throws ValidationException {
		functionValidator.validate(function);
		return functionService.save(function);
	}
}
