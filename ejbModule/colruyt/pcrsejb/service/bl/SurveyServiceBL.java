package colruyt.pcrsejb.service.bl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.entity.competence.BehavioralCompetence;
import colruyt.pcrsejb.entity.competence.Competence;
import colruyt.pcrsejb.entity.competence.CraftCompetence;
import colruyt.pcrsejb.entity.competence.DomainCompetence;
import colruyt.pcrsejb.entity.competence.LeveledCompetence;
import colruyt.pcrsejb.entity.competence.OperatingUnitCompetence;
import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.entity.role.Role;
import colruyt.pcrsejb.entity.survey.ConsensusRating;
import colruyt.pcrsejb.entity.survey.Rating;
import colruyt.pcrsejb.entity.survey.Survey;
import colruyt.pcrsejb.entity.survey.SurveyKind;
import colruyt.pcrsejb.entity.survey.SurveySet;
import colruyt.pcrsejb.entity.team.Team;
import colruyt.pcrsejb.entity.user.User;
import colruyt.pcrsejb.service.dl.rating.JPARatingService;
import colruyt.pcrsejb.service.dl.survey.JPASurveySetService;
import colruyt.pcrsejb.service.dl.team.JPATeamService;
import colruyt.pcrsejb.service.dl.user.JPAUserService;
import colruyt.pcrsejb.util.exceptions.bl.UserIsNotMemberOfTeamException;

@Stateless
public class SurveyServiceBL {

	@EJB
	private JPASurveySetService surveyService;
    //private SurveySetService surveyService = new MemorySurveySetService();
	@EJB
	private JPATeamService teamService;
    //private TeamServiceBL teamService =  new TeamServiceBL();
	@EJB
	private JPAUserService userService;
    //private UserServiceBL userService = new UserServiceBL();
	@EJB
    private JPARatingService ratingService;

    public List<SurveySet> findSurveySetsByUser(User u){

            return surveyService.findSurveySetsByUser(u);

    }

    public SurveySet findSurveySetsByUserAndYear(User u , LocalDate year){
        return surveyService.findSurveySetsByUserAndDate(u,year);
    }

    public SurveySet save(SurveySet toSave){
        //TOdo:Validate ?? wat is een geldige Survey

          return surveyService.save(toSave);
    }


    public void notifyTeamManager(){

        //Todo: Event ???
    }
    
    
    public SurveySet createSurveySetFor(User user) throws UserIsNotMemberOfTeamException {
    	
    	//Get Team of user
    	Team team = teamService.getTeam(user);
    	//Get TeamManager of User
    	User teamManager = teamService.getOwnerOfTeam(team);
    	
    	//Get Ratings For Functie .... 
    	Function func = this.userService.getFunctionForPerson(user);
    	
    	List<Rating> rat = getRatings(func);

    	SurveySet set = new SurveySet();
    	
    	Survey member = new Survey();
    	member.setRatingList(rat);
    	member.setSurveyKind(SurveyKind.TeamMember);
    	
    	
    	Survey manager = new Survey();
    	manager.setRatingList(rat);
    	manager.setSurveyKind(SurveyKind.TeamManager);
    	
    	Survey consensus = new Survey();
    	consensus.setRatingList(this.getConsensusRatings(func));
    	consensus.setSurveyKind(SurveyKind.Consensus);
    	
    	set.getSurveyList().add(member);
    	
    	set.getSurveyList().add(manager);
    	
    	set.getSurveyList().add(consensus );
	   
    	
    	return set;
    	
    }
    
    private List<Rating> getConsensusRatings(Function func){
    	List<Rating> rat = new ArrayList<Rating>();
        	
        	//Add Alle Competences 
    	   for(BehavioralCompetence fucomp :  func.getBehavioralCompetenceSet()) {
    		   
    		   if(fucomp instanceof LeveledCompetence) {
    			   LeveledCompetence lc = (LeveledCompetence) fucomp;
    			   //TODO ASK THOMAS
    			   //rat.add(new ConsensusRating(lc.getMinLevel(),false,(Competence)lc,""));
    			   
    			   
    		   }
    		   
    		 
    		   
    	   }
        
    	   //Competences Voor Rollen
    	   for(Role rol: func.getRoleSet()) {
    		   for(CraftCompetence rolecomp : rol.getCraftCompetenceList()) {
    			   
    			   rat.add(new ConsensusRating(0,false,(Competence)rolecomp,""));
    			   
    		   }
    			   
    		   
    	    }
    	   
    	   //DomainCompetences
    	   for(DomainCompetence domcomp : this.ratingService.getAllDomainCompetences()) {
    		   
    		   
    		   rat.add(new ConsensusRating(0,false,(Competence)domcomp,""));
    		   
    	   }
    	   
    	 //DomainCompetences
    	   for(OperatingUnitCompetence oucomp : this.ratingService.getAllOperatingUnitCompetencesFor(func.getOperatingUnit())) {
    		   
    		   
    		   rat.add(new ConsensusRating(0,false,(Competence)oucomp,""));
    		   
    	   }
    	   return rat;
        	
        }
    
    private List<Rating> getRatings(Function func ){
	List<Rating> rat = new ArrayList<Rating>();
    	
    	//Add Alle Competences 
	   for(BehavioralCompetence fucomp :  func.getBehavioralCompetenceSet()) {
		   
		   if(fucomp instanceof LeveledCompetence) {
			   LeveledCompetence lc = (LeveledCompetence) fucomp;
			   //TODO ASK THOMAS
			   //rat.add(new Rating(lc.getMinLevel(),false,(Competence)lc));
			   
			   
		   }
		   
		 
		   
	   }
    
	   //Competences Voor Rollen
	   for(Role rol: func.getRoleSet()) {
		   for(CraftCompetence rolecomp : rol.getCraftCompetenceList()) {
			   
			   rat.add(new Rating(0,false,(Competence)rolecomp));
			   
		   }
			   
		   
	    }
	   
	   //DomainCompetences
	   for(DomainCompetence domcomp : this.ratingService.getAllDomainCompetences()) {
		   
		   
		   rat.add(new Rating(0,false,(Competence)domcomp));
		   
	   }
	   
	 //DomainCompetences
	   for(OperatingUnitCompetence oucomp : this.ratingService.getAllOperatingUnitCompetencesFor(func.getOperatingUnit())) {
		   
		   
		   rat.add(new Rating(0,false,(Competence)oucomp));
		   
	   }
	   return rat;
    	
    }
    

	public User findUserBySurvey(Survey survey) {
		return surveyService.findUserBySurvey(survey);
	}

	public SurveySet getLastSurveySetForUser(User user) {
		return surveyService.findLastSurveySetForUser(user);
	}






}
