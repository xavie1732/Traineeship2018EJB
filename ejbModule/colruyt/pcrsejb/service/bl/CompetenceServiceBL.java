package colruyt.pcrsejb.service.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.entity.competence.BehavioralCompetence;
import colruyt.pcrsejb.entity.competence.Competence;
import colruyt.pcrsejb.service.dl.competence.JPACompetenceService;


@Stateless
public class CompetenceServiceBL {

	@EJB
	private JPACompetenceService competencedb = new JPACompetenceService();
	//private CompetenceService competencedb = new DbCompetenceService();



	public Collection<Competence> getAllCompetences() {
		return competencedb.getAllElements();
	}

	public void delete(Competence domain) {
		competencedb.deleteElement(domain);
	}

	public Competence getCompetence(Competence domain) {
		return competencedb.getElement(domain);
	}

	public List<BehavioralCompetence> getAllBehavioralCompetences() {
		List<BehavioralCompetence> listOfCompetences= new ArrayList<>();
		for(Competence competence : competencedb.getAllElements() )
		{
			if(competence instanceof BehavioralCompetence)
			{
				listOfCompetences.add((BehavioralCompetence) competence);
			}
		}
		return listOfCompetences;
	}

	public Competence saveCompetence(Competence competence) {
		return competencedb.save(competence);
	}
}

