package colruyt.pcrsejb.service.bl;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.entity.domain.Domain;
import colruyt.pcrsejb.service.dl.domain.JPADomainService;

@Stateless
public class DomainServiceBL {
	@EJB
	private JPADomainService domaindb;
	
	// Altijd op Abstract werken.
	//private DomainService domaindb = new JPADomainService();

	public Domain saveDomain(Domain domain){
		return domaindb.save(domain);
	}

	public Collection<Domain> getAllDomains() {
		return domaindb.getAllElements();
	}

	public void delete(Domain domain) {
		domaindb.deleteElement(domain);
	}

	public Domain getDomain(Domain domain) {
		return domaindb.getElement(domain);
	}
}
