package colruyt.pcrsejb.converter.enrolment;

import colruyt.pcrsejb.bo.enrolment.EnrolmentBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.converter.role.RoleBoConverter;
import colruyt.pcrsejb.converter.user.UserBoConverter;
import colruyt.pcrsejb.converter.userPrivilege.UserPrivilegeBoConverter;
import colruyt.pcrsejb.entity.enrolment.Enrolment;

public class EnrolmentBoConverter implements GenericConverter<Enrolment, EnrolmentBo>{

	UserBoConverter userConverter = new UserBoConverter();
	UserPrivilegeBoConverter userPrivilegeConverter= new UserPrivilegeBoConverter();
	RoleBoConverter roleConverter = new RoleBoConverter();

	@Override
	public Enrolment convertTo(EnrolmentBo from) {
		Enrolment bo = new Enrolment();
		bo.setId(from.getId());
		bo.setActive(from.isActive());
		bo.setPrivilege(userPrivilegeConverter.convertTo(from.getPrivilegeBo()));
		bo.setUser(userConverter.convertTo(from.getUserBo()));
		bo.setRole(roleConverter.convertTo(from.getRoleBo()));
		return bo;
	}

	
}
