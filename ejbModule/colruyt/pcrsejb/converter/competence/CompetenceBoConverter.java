package colruyt.pcrsejb.converter.competence;

import java.util.ArrayList;
import java.util.List;

import colruyt.pcrsejb.bo.competence.BehavioralCompetenceBo;
import colruyt.pcrsejb.bo.competence.CompetenceBo;
import colruyt.pcrsejb.bo.competence.CompetenceLevelBo;
import colruyt.pcrsejb.bo.competence.CraftCompetenceBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.entity.competence.*;

public class CompetenceBoConverter implements GenericConverter<Competence,CompetenceBo> {

	@Override
	public Competence convertTo(CompetenceBo from) {
		
		Competence competence = null;
        switch(determineInstance(from)){
        case "BehavioralCompetenceBo" : competence = new BehavioralCompetence(from.getId(), from.getName(), ( (BehavioralCompetenceBo) from).getDescription(), getPosibilities(((BehavioralCompetenceBo) from).getPosibilities()) ); break;
        case "CraftCompetenceBo" : competence = new CraftCompetence(from.getId(), from.getName(), ( (CraftCompetenceBo) from).getDescription(), getPosibilities(((CraftCompetenceBo) from).getPosibilities())); break;
        case "DomainCompetenceBo" : competence = new DomainCompetence(from.getId(), from.getName()); break;
        case "OperatingUnitCompetenceBo" : competence = new OperatingUnitCompetence(from.getId(), from.getName()); break;
        }
        return competence;   
	}

	private List<CompetenceLevel> getPosibilities(List<CompetenceLevelBo> boPosibilities) {
		List<CompetenceLevel> posibilities = new ArrayList<>();
		for (CompetenceLevelBo level : boPosibilities) {
			CompetenceLevel l = new CompetenceLevel();
			l.setDescription(level.getDescription());
			l.setId(level.getId());
			l.setMinLevel(level.getMinLevel());
			l.setOrderLevel(level.getOrderLevel());
			posibilities.add(l);
		}
		return posibilities;
	}

	private String determineInstance(CompetenceBo from) {
		return from.getClass().getSimpleName();
	}

}
