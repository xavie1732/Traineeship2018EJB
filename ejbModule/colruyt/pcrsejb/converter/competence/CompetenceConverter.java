package colruyt.pcrsejb.converter.competence;

import java.util.ArrayList;
import java.util.List;

import colruyt.pcrsejb.bo.competence.BehavioralCompetenceBo;
import colruyt.pcrsejb.bo.competence.CompetenceBo;
import colruyt.pcrsejb.bo.competence.CompetenceLevelBo;
import colruyt.pcrsejb.bo.competence.DomainCompetenceBo;
import colruyt.pcrsejb.bo.competence.OperatingUnitCompetenceBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.entity.competence.BehavioralCompetence;
import colruyt.pcrsejb.entity.competence.Competence;
import colruyt.pcrsejb.entity.competence.CompetenceLevel;
import colruyt.pcrsejb.entity.competence.LeveledCompetence;

public class CompetenceConverter implements GenericConverter<CompetenceBo,Competence> {

	@Override
	public CompetenceBo convertTo(Competence from) {
		
		CompetenceBo competenceBo = null;
        switch(determineInstance(from)){
        case "BehavioralCompetence" : competenceBo = new BehavioralCompetenceBo(from.getId(), from.getName(), ((LeveledCompetence) from).getDescription(), getBoPosibilities(((BehavioralCompetence) from).getPosibilities() )); break;
        // TODO Solve error
        //case "CraftCompetence" : competenceBo = new CraftCompetenceBo(from.getId(), from.getName(), ((LeveledCompetence) from).getDescription(), getBoPosibilities(((CraftCompetence) from).getPosibilities() )); break;
        case "DomainCompetence" : competenceBo = new DomainCompetenceBo(from.getId(), from.getName()); break;
        case "OperatingUnitCompetence" : competenceBo = new OperatingUnitCompetenceBo(from.getId(), from.getName()); break;
        }
        return competenceBo;   
	}

	private List<CompetenceLevelBo> getBoPosibilities(List<CompetenceLevel> Posibilities) {
		List<CompetenceLevelBo> boposibilities = new ArrayList<>();
		for (CompetenceLevel level : Posibilities) {
			CompetenceLevelBo l = new CompetenceLevelBo();
			l.setDescription(level.getDescription());
			l.setId(level.getId());
			l.setMinLevel(level.getMinLevel());
			l.setOrderLevel(level.getOrderLevel());
			boposibilities.add(l);
		}
		return boposibilities;
	}

	private String determineInstance(Competence from) {
		return from.getClass().getSimpleName();
	}

}
