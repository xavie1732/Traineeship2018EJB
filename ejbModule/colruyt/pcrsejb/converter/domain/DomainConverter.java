package colruyt.pcrsejb.converter.domain;

import colruyt.pcrsejb.bo.domain.DomainBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.entity.domain.Domain;

/**
 * Class for converting a domain Entity into a domain BO
 */
public class DomainConverter implements GenericConverter<DomainBo,Domain> {

    @Override
    public DomainBo convertTo(Domain from) {
    	DomainBo domainBo = new DomainBo(from.getId(),from.getName());
       return domainBo;
    }
    
}
