package colruyt.pcrsejb.converter.domain;

import colruyt.pcrsejb.bo.domain.DomainBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.entity.domain.Domain;

public class DomainBoConverter implements GenericConverter<Domain,DomainBo> {

    @Override
    public Domain convertTo(DomainBo from) {
    	return new Domain(from.getId(), from.getName());
    }
}
