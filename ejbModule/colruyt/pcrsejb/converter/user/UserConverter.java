package colruyt.pcrsejb.converter.user;

import java.util.HashSet;
import java.util.Set;

import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.bo.userPrivilege.UserPrivilegeBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.converter.userPrivilege.UserPrivilegeConverter;
import colruyt.pcrsejb.entity.user.User;
import colruyt.pcrsejb.entity.userPrivilege.UserPrivilege;

/**
 * Class for converting a user Entity into a user BO
 */
public class UserConverter implements GenericConverter<UserBo,User> {


    @Override
    public UserBo convertTo(User from) {
    	Integer id = from.getId();
    	String fName = from.getFirstName();
    	String lName = from.getLastName();
    	String mail = from.getEmail();
    	String pass = from.getPassword();
    	Set<UserPrivilege> privs = from.getPrivileges();
    	String country = from.getCountry();
    	System.out.println("HIER");
    	UserBo userBo = new UserBo(id,fName, lName, mail, pass, getPrivileges(privs), country);
       return userBo;
    }
    
    private HashSet<UserPrivilegeBo> getPrivileges(Set<UserPrivilege> hashSet) {
    	System.out.println("OOK HIER");
    	HashSet<UserPrivilegeBo> privileges = new HashSet<>();
		UserPrivilegeConverter conv = new UserPrivilegeConverter();
    	for (UserPrivilege pr : hashSet) {
    		privileges.add(conv.convertTo(pr));
    	}
    	System.out.println("KLAAR");
    	return privileges;
    }

    
}
