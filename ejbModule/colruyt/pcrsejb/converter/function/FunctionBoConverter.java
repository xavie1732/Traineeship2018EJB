package colruyt.pcrsejb.converter.function;

import java.util.HashSet;
import java.util.Set;

import colruyt.pcrsejb.bo.competence.BehavioralCompetenceBo;
import colruyt.pcrsejb.bo.competence.CompetenceBo;
import colruyt.pcrsejb.bo.function.FunctionBo;
import colruyt.pcrsejb.bo.role.RoleBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.converter.competence.CompetenceBoConverter;
import colruyt.pcrsejb.converter.operatingunit.OperatingUnitBoConverter;
import colruyt.pcrsejb.converter.role.RoleBoConverter;
import colruyt.pcrsejb.entity.competence.BehavioralCompetence;
import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.entity.role.Role;

public class FunctionBoConverter implements GenericConverter<Function,FunctionBo> {

    private OperatingUnitBoConverter operatingUnitBoConverter = new OperatingUnitBoConverter();

    @Override
    public Function convertTo(FunctionBo from) {
        return new Function(from.getId(), from.getTitle(), getRoleSet(from.getRoleBoSet()), getBehavioralCompetenceSet(from.getBehavioralCompetenceBoSet()), operatingUnitBoConverter.convertTo(from.getOperatingUnitBo()));
    }

    private HashSet<Role> getRoleSet(Set<RoleBo> roleBoSet){
        HashSet<Role> roleHashSet = new HashSet<>();
        RoleBoConverter conv = new RoleBoConverter();
        if (roleBoSet != null) {
            for(RoleBo roleBo : roleBoSet) {
                roleHashSet.add(conv.convertTo(roleBo));
            }
        }
        return roleHashSet;
    }

    private HashSet<BehavioralCompetence> getBehavioralCompetenceSet(Set<BehavioralCompetenceBo> behavioralCompetenceBoSet){
        HashSet<BehavioralCompetence> behavioralCompetenceHashSet = new HashSet<>();
		CompetenceBoConverter competenceBoConverter = new CompetenceBoConverter();
		if (behavioralCompetenceBoSet != null) {
            for(BehavioralCompetenceBo behavioralCompetenceBo: behavioralCompetenceBoSet) {
            	behavioralCompetenceHashSet.add((BehavioralCompetence) competenceBoConverter.convertTo((CompetenceBo) behavioralCompetenceBo));
            }
        }
        return behavioralCompetenceHashSet;
    }
}
