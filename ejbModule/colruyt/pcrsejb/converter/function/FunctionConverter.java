package colruyt.pcrsejb.converter.function;

import java.util.HashSet;
import java.util.Set;

import colruyt.pcrsejb.bo.competence.BehavioralCompetenceBo;
import colruyt.pcrsejb.bo.function.FunctionBo;
import colruyt.pcrsejb.bo.operatingunit.OperatingUnitBo;
import colruyt.pcrsejb.bo.role.RoleBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.converter.competence.CompetenceConverter;
import colruyt.pcrsejb.converter.operatingunit.OperatingUnitConverter;
import colruyt.pcrsejb.converter.role.RoleConverter;
import colruyt.pcrsejb.entity.competence.BehavioralCompetence;
import colruyt.pcrsejb.entity.competence.Competence;
import colruyt.pcrsejb.entity.competence.FunctionCompetence;
import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.entity.operatingunit.OperatingUnit;
import colruyt.pcrsejb.entity.role.Role;

public class FunctionConverter implements GenericConverter<FunctionBo, Function> {

    private OperatingUnitConverter operatingUnitConverter = new OperatingUnitConverter();

    @Override
    public FunctionBo convertTo(Function from) {
        FunctionBo functionBo = null;
        if (from != null) {
            functionBo = new FunctionBo(from.getId(), from.getTitle(), getRoleBoHashSet(from.getRoleSet()), getBehavioralCompetenceBoHashSet(from.getBehavioralCompetenceSet()), getOperatingUnit(from.getOperatingUnit()));
        }
        return functionBo;
    }

    private HashSet<RoleBo> getRoleBoHashSet(Set<Role> roles) {
        HashSet<RoleBo> roleBoHashSet = new HashSet<>();
        RoleConverter conv = new RoleConverter();
        for (Role role : roles) {
            roleBoHashSet.add(conv.convertTo(role));
        }
        return roleBoHashSet;
    }

    private HashSet<BehavioralCompetenceBo> getBehavioralCompetenceBoHashSet(Set<BehavioralCompetence> competences) {
        HashSet<BehavioralCompetenceBo> behavioralCompetenceBoHashSet = new HashSet<>();
        CompetenceConverter conv = new CompetenceConverter();
        for (FunctionCompetence behavioralCompetence : competences) {
            behavioralCompetenceBoHashSet.add((BehavioralCompetenceBo) conv.convertTo((Competence) behavioralCompetence));
        }
        return behavioralCompetenceBoHashSet;
    }

    private OperatingUnitBo getOperatingUnit(OperatingUnit ou) {
        return operatingUnitConverter.convertTo(ou);
    }

}
