package colruyt.pcrsejb.converter.survey;

import java.util.ArrayList;
import java.util.List;

import colruyt.pcrsejb.bo.survey.SurveyBo;
import colruyt.pcrsejb.bo.survey.SurveySetBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.entity.survey.Survey;
import colruyt.pcrsejb.entity.survey.SurveySet;

public class SurveySetBoConverter implements GenericConverter<SurveySet, SurveySetBo> {


    @Override
    public SurveySet convertTo(SurveySetBo from) {
        SurveyBoConverter2 surveyConverter = new SurveyBoConverter2();
        List<Survey> surveys = new ArrayList<>();
        for(SurveyBo s : from.getSurveyBoList()){
        	surveys.add(surveyConverter.convertTo(s));
        }
        return new SurveySet(from.getId(), from.getSurveyYear(), surveys);
    }

}
