package colruyt.pcrsejb.converter.survey;

import java.util.ArrayList;
import java.util.List;

import colruyt.pcrsejb.bo.survey.SurveyBo;
import colruyt.pcrsejb.bo.survey.SurveySetBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.entity.survey.Survey;
import colruyt.pcrsejb.entity.survey.SurveySet;

public class SurveySetConverter implements GenericConverter<SurveySetBo, SurveySet> {

        @Override
        public SurveySetBo convertTo(SurveySet from) {
            List<SurveyBo> surveys = new ArrayList<>();
            SurveyConverter2 surveyConverter = new SurveyConverter2();
            
            for(Survey s : from.getSurveyList()){
            	surveys.add(surveyConverter.convertTo(s));
            }
            return new SurveySetBo(from.getId(), from.getSurveyYear(), surveys);
        }

}
