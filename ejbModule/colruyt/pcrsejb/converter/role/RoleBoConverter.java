package colruyt.pcrsejb.converter.role;

import java.util.ArrayList;
import java.util.List;

import colruyt.pcrsejb.bo.competence.CompetenceBo;
import colruyt.pcrsejb.bo.competence.CraftCompetenceBo;
import colruyt.pcrsejb.bo.role.RoleBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.converter.competence.CompetenceBoConverter;
import colruyt.pcrsejb.entity.competence.CraftCompetence;
import colruyt.pcrsejb.entity.role.Role;

public class RoleBoConverter implements GenericConverter<Role,RoleBo> {


    @Override
    public Role convertTo(RoleBo from) {
    	return new Role(from.getId(), from.getName(), getCraftComptenceList(from.getCraftCompetenceBoList()));
    }

    private List<CraftCompetence> getCraftComptenceList(List<CraftCompetenceBo> competences) {
        List<CraftCompetence> craftCompetenceList = new ArrayList<>();
        if(competences != null) {
            CompetenceBoConverter conv = new CompetenceBoConverter();
            for (CraftCompetenceBo craftCompetenceBo : competences) {
                craftCompetenceList.add((CraftCompetence) conv.convertTo((CompetenceBo) craftCompetenceBo));
            }
        }
        return craftCompetenceList;
    }
}
