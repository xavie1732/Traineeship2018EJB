package colruyt.pcrsejb.converter.role;

import java.util.ArrayList;
import java.util.List;

import colruyt.pcrsejb.bo.competence.CraftCompetenceBo;
import colruyt.pcrsejb.bo.role.RoleBo;
import colruyt.pcrsejb.converter.GenericConverter;
import colruyt.pcrsejb.entity.competence.CraftCompetence;
import colruyt.pcrsejb.entity.role.Role;

public class RoleConverter implements GenericConverter<RoleBo,Role> {


    @Override
    public RoleBo convertTo(Role from) {
        if (from != null) {
            return new RoleBo(from.getId(), from.getName(), getCraftCompetenceBoList(from.getCraftCompetenceList()));
        }
        return null;
    }

    private List<CraftCompetenceBo> getCraftCompetenceBoList(List<CraftCompetence> competences){
        List<CraftCompetenceBo> craftCompetenceBos = new ArrayList<>();
        //for (RoleCompetence roleCompetence : competences) {
            //if (roleCompetence instanceof CraftCompetence) {
                //CraftCompetenceConverter conv = new CraftCompetenceConverter();
                //oleCompetenceBos.add( conv.convertTo( (CraftCompetence) roleCompetence ));
            //}
        //}
        return craftCompetenceBos;
    }
}
