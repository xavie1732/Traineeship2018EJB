package colruyt.pcrsejb.facade;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.bo.enrolment.EnrolmentBo;
import colruyt.pcrsejb.converter.enrolment.EnrolmentBoConverter;
import colruyt.pcrsejb.converter.enrolment.EnrolmentConverter;
import colruyt.pcrsejb.service.bl.EnrolmentBL;

@Stateless
public class EnrolmentFacade implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
    EnrolmentBL enrolmentBL;
    EnrolmentBoConverter enrolmentBoConverter = new EnrolmentBoConverter();
    EnrolmentConverter enrolmentConverter = new EnrolmentConverter();


    public EnrolmentBo saveEnrolment(EnrolmentBo enrolmentBo) {
        return enrolmentConverter.convertTo(enrolmentBL.save(enrolmentBoConverter.convertTo(enrolmentBo)));
    }
}
