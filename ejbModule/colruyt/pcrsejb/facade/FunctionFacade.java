package colruyt.pcrsejb.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.bo.function.FunctionBo;
import colruyt.pcrsejb.converter.function.FunctionBoConverter;
import colruyt.pcrsejb.converter.function.FunctionConverter;
import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.service.bl.FunctionServiceBL;
import colruyt.pcrsejb.util.exceptions.validation.ValidationException;

@Stateless
public class FunctionFacade {

    @EJB
    private FunctionServiceBL functionServiceBL;

    private FunctionConverter functionConverter = new FunctionConverter();
    private FunctionBoConverter functionBoConverter = new FunctionBoConverter();


    public List<FunctionBo> getAllFunctionNames() {
        List<FunctionBo> functionBoList = new ArrayList<>();

        for(Function function : functionServiceBL.getAllFunctionNames()) {
            functionBoList.add(functionConverter.convertTo(function));
        }
        return functionBoList;
    }

    public FunctionBo getFunction(FunctionBo functionBo) {
        Function f = functionBoConverter.convertTo(functionBo);
        Function returned = functionServiceBL.getFunction(f);
        return functionConverter.convertTo(returned);
    }
    
    
	public FunctionBo saveFunction(FunctionBo functionBo) {
		try {
			return functionConverter.convertTo(functionServiceBL.saveFunction(functionBoConverter.convertTo(functionBo)));
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	
	}
}
