package colruyt.pcrsejb.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.bo.competence.CompetenceBo;
import colruyt.pcrsejb.converter.competence.CompetenceBoConverter;
import colruyt.pcrsejb.converter.competence.CompetenceConverter;
import colruyt.pcrsejb.entity.competence.Competence;
import colruyt.pcrsejb.service.bl.CompetenceServiceBL;

@Stateless
public class CompetenceFacade {
	
	@EJB
	private CompetenceServiceBL competenceServiceBL;
	private CompetenceBoConverter competenceBoConverter = new CompetenceBoConverter();
	private CompetenceConverter competenceConverter = new CompetenceConverter();
	
	public List<CompetenceBo> getAllBehavioralCompetences()
	{
		List<CompetenceBo> listOfCompetences = new ArrayList<>();
		for(Competence competence : competenceServiceBL.getAllCompetences())
		{
			listOfCompetences.add(competenceConverter.convertTo(competence));
		}
		return listOfCompetences;
	}

	public void removeCompetence(CompetenceBo competence) {
		competenceServiceBL.delete(competenceBoConverter.convertTo(competence));
	}

	public CompetenceBo saveCompetence(CompetenceBo competence) {
			return competenceConverter.convertTo(competenceServiceBL.saveCompetence(competenceBoConverter.convertTo(competence)));

	}

}
