package colruyt.pcrsejb.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.bo.function.FunctionBo;
import colruyt.pcrsejb.bo.role.RoleBo;
import colruyt.pcrsejb.converter.function.FunctionBoConverter;
import colruyt.pcrsejb.converter.role.RoleBoConverter;
import colruyt.pcrsejb.converter.role.RoleConverter;
import colruyt.pcrsejb.entity.role.Role;
import colruyt.pcrsejb.service.bl.RoleServiceBL;

@Stateless
public class RoleFacade {

	@EJB
	private RoleServiceBL roleServiceBL;
	private RoleBoConverter roleBoConverter = new RoleBoConverter();
	private RoleConverter roleConverter = new RoleConverter();

	/**
	 * Methode die een lijst van alle roleBo's terug geeft voor een bepaalde functie
	 */
	public List<RoleBo> getAllRolesForFunction(FunctionBo functionBo) {
		List<RoleBo> roleBoList = new ArrayList<>();
		FunctionBoConverter conv = new FunctionBoConverter();
		for (Role role : roleServiceBL.getAllRolesForFunction(conv.convertTo(functionBo))){
			roleBoList.add(roleConverter.convertTo(role));
		}
		return roleBoList;
	}

	/**
	 * Methode die een lijst van alle roleBo's terug geeft
	 */
	public List<RoleBo> getAllRoles() {
		List<RoleBo> roles = new ArrayList<>();
		for (Role r : roleServiceBL.getAllRoles()) {
			roles.add(roleConverter.convertTo(r));
		}
		return roles;
	}
	
    public void removeRole(RoleBo role) {
		roleServiceBL.delete(roleBoConverter.convertTo(role));
	}
	
	public RoleBo saveRole(RoleBo role) {
		return roleConverter.convertTo(roleServiceBL.saveRole(roleBoConverter.convertTo(role)));
	}

	public RoleBo getRole(RoleBo roleBo) {
		return roleConverter.convertTo(roleServiceBL.getRole(roleBoConverter.convertTo(roleBo)));
	}
}
