package colruyt.pcrsejb.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import colruyt.pcrsejb.bo.domain.DomainBo;
import colruyt.pcrsejb.converter.domain.DomainBoConverter;
import colruyt.pcrsejb.converter.domain.DomainConverter;
import colruyt.pcrsejb.entity.domain.Domain;
import colruyt.pcrsejb.service.bl.DomainServiceBL;

@Stateless
public class DomainFacade {

	@EJB
	private DomainServiceBL domainServiceBL;
	private DomainBoConverter domainBoConverter = new DomainBoConverter();
	private DomainConverter domainConverter = new DomainConverter();

	/**
	 * Methode die een lijst van alle domainBo's terug geeft
	 */
	public List<DomainBo> getAllDomains() {
		List<DomainBo> domains = new ArrayList<>();
		for (Domain d : domainServiceBL.getAllDomains()) {
			domains.add(domainConverter.convertTo(d));
		}
		return domains;
	}

	/**
	 * Methode die een domainBo object ontvangt en een domain object doorgeeft zodat
	 * deze kan verwijderd worden
	 * 
	 * @param domain
	 */
	public void removeDomain(DomainBo domain) {
		domainServiceBL.delete(domainBoConverter.convertTo(domain));
	}
	
	public DomainBo saveDomain(DomainBo domain) {
		return domainConverter.convertTo(domainServiceBL.saveDomain(domainBoConverter.convertTo(domain)));
	}

	public DomainBo getDomain(DomainBo domainBo) {
		return domainConverter.convertTo(domainServiceBL.getDomain(domainBoConverter.convertTo(domainBo)));
	}
}
