package colruyt.pcrsejb.bo.survey;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class SurveySetBo implements Comparable<SurveySetBo>, Serializable{
    
	private static final long serialVersionUID = 1L;

	private Integer id;
	
    private LocalDate surveyYear;
	
    private List<SurveyBo> surveyList = new ArrayList<>();

    public SurveySetBo(Integer id, LocalDate surveyYear, List<SurveyBo> surveyList) {
        this.id = id;
        this.surveyYear = surveyYear;
        this.surveyList = surveyList;
    }

    public SurveySetBo() {
    	
    }

    @Override
    public int compareTo(SurveySetBo o) {
        return this.surveyYear.compareTo(o.getSurveyYear());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getSurveyYear() {
        return surveyYear;
    }

    public void setSurveyYear(LocalDate surveyYear) {
        this.surveyYear = surveyYear;
    }

	public List<SurveyBo> getSurveyBoList() {
		return surveyList;
	}

	public void setSurveyBoList(List<SurveyBo> surveySet) {
		this.surveyList = surveySet;
	}

}
