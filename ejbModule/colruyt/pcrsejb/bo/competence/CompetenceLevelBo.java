package colruyt.pcrsejb.bo.competence;

import java.io.Serializable;


public class CompetenceLevelBo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer Id;
	private Boolean minLevel;
	private String description;
	private Integer orderLevel;
	public CompetenceLevelBo() {
		super();
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public Boolean getMinLevel() {
		return minLevel;
	}
	public void setMinLevel(Boolean minLevel) {
		this.minLevel = minLevel;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getOrderLevel() {
		return orderLevel;
	}
	public void setOrderLevel(Integer orderLevel) {
		this.orderLevel = orderLevel;
	} 
}
