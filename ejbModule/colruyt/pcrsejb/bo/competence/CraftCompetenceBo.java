package colruyt.pcrsejb.bo.competence;

public class CraftCompetenceBo extends LeveledCompetenceBo implements RoleCompetenceBo {

	public CraftCompetenceBo() {
		super();
	}

	public CraftCompetenceBo(String name, String description){
		super(name, description);
	}

	public CraftCompetenceBo(int id, String name, String description) {
		super(id, name, description);
	}


	public CraftCompetenceBo(Integer id, String name, String description, CompetenceTypeBo posibilities) {
		super(id, name, posibilities, description);
	}

}
