package colruyt.pcrsejb.bo.competence;

import java.util.ArrayList;
import java.util.List;

public abstract class LeveledCompetenceBo extends CompetenceBo{


	private String description;
	private List<CompetenceLevelBo> posibilities = new ArrayList<CompetenceLevelBo>();

    public LeveledCompetenceBo() {
        super();
    }

    public LeveledCompetenceBo(CompetenceTypeBo competenceBoType) {
		super(competenceBoType);
		fillPossibilities();
	}



	public LeveledCompetenceBo(Integer id, String name, CompetenceTypeBo competenceBoType, String description) {
		super(id, name, competenceBoType);
		this.description = description;
		this.posibilities = new ArrayList<>();
		fillPossibilities();
	}



	public LeveledCompetenceBo(Integer id, String name, String description)
    {
    	super(id, name);
    	this.setDescription(description);
    }
    
    public LeveledCompetenceBo(String name, String description)
    {
    	super(name);
    	this.setDescription(description);
    }

    

	public LeveledCompetenceBo(Integer Id, String name, String description, CompetenceTypeBo competenceTypeBo ,List<CompetenceLevelBo> posibilities) {
		super(Id, name, competenceTypeBo);
		this.description = description;
		this.posibilities = posibilities;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<CompetenceLevelBo> getPosibilities() {
		return posibilities;
	}

	public void setPosibilities(List<CompetenceLevelBo> posibilities) {
		this.posibilities = posibilities;
	}   
	
	private void fillPossibilities()
	{
		for(int i = 0; i<this.getCompetenceTypeBo().getLevelsAmount(); i++)
		{
			posibilities.add(new CompetenceLevelBo());
		}
	}
	
    
}
