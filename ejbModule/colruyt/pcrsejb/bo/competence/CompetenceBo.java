package colruyt.pcrsejb.bo.competence;

public abstract class CompetenceBo {

	private Integer id;
    private String name;
    private CompetenceTypeBo competenceTypeBo;
    
	public CompetenceBo() {
		super();
	}

	public CompetenceBo(CompetenceTypeBo competenceBoType) {
		super();
		this.competenceTypeBo = competenceBoType;
	}



	public CompetenceBo(String name)  {
		super();
		this.setName(name);
	}

	public CompetenceBo(Integer id, String name) {
		this.setId(id);
		this.setName(name);
	}

	public CompetenceBo(Integer id, String name, CompetenceTypeBo competenceBoType) {
		super();
		this.id = id;
		this.name = name;
		this.competenceTypeBo = competenceBoType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name)  {
		this.name = name;
	}

	public CompetenceTypeBo getCompetenceTypeBo() {
		return competenceTypeBo;
	}

	public void setCompetenceTypeBo(CompetenceTypeBo competenceBoType) {
		this.competenceTypeBo = competenceBoType;
	}

	@Override
	public int hashCode() {
		final Integer prime = 31;
		Integer result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompetenceBo other = (CompetenceBo) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


}
