package colruyt.pcrsejb.bo.competence;

import java.util.List;

public class BehavioralCompetenceBo extends LeveledCompetenceBo implements FunctionCompetenceBo {

	public BehavioralCompetenceBo() {
		super(CompetenceTypeBo.FIVELEVELSENERGYRATEDINTERESTRATED);
	}

	public BehavioralCompetenceBo(String name, String description){
		super(null, name, CompetenceTypeBo.FIVELEVELSENERGYRATEDINTERESTRATED, description);
	}

	public BehavioralCompetenceBo(Integer id, String name, String description) {
		super(id, name, CompetenceTypeBo.FIVELEVELSENERGYRATEDINTERESTRATED, description);
	}

	public BehavioralCompetenceBo(Integer id, String name, String description, List<CompetenceLevelBo> posibilities) {
		super(id, name, description, CompetenceTypeBo.FIVELEVELSENERGYRATEDINTERESTRATED, posibilities);
	}
		
}
