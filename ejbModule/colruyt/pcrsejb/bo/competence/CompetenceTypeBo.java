package colruyt.pcrsejb.bo.competence;

public enum CompetenceTypeBo {
	
	FIVELEVELSENERGYRATEDINTERESTRATED(5, true, true);
	
	private Integer levelsAmount;
	private boolean energyRated;
	private boolean interestRated;

	private CompetenceTypeBo(Integer levelsAmount, boolean energyRated, boolean interestRated) {
		this.levelsAmount = levelsAmount;
		this.energyRated = energyRated;
		this.interestRated = interestRated;
	}

	public Integer getLevelsAmount() {
		return levelsAmount;
	}

	public boolean isEnergyRated() {
		return energyRated;
	}

	public boolean isInterestRated() {
		return interestRated;
	}
}
