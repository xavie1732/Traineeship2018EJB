package colruyt.pcrsejb.bo.team;

import java.util.HashSet;
import java.util.Set;

import colruyt.pcrsejb.bo.enrolment.EnrolmentBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.bo.userPrivilege.PrivilegeTypeBo;
import colruyt.pcrsejb.bo.userPrivilege.UserPrivilegeBo;

/**
 * Klasse voor het aanmaken van een Team.
 * 
 * @author jda1mbw
 */
public class TeamBo {

	private String name;
	private Integer id;
	private Set<EnrolmentBo> enrolmentsBoSet;

	public TeamBo() {}
	/**
	 * Constructor voor het aanmaken van een Team
	 *
	 * @param name
	 * @param teamManager
	 */
	public TeamBo(String name, UserBo teamManager) {
		
		setName(name);

		EnrolmentBo enrolment = new EnrolmentBo(teamManager, new UserPrivilegeBo(PrivilegeTypeBo.TEAMMANAGER, true), true);
		enrolmentsBoSet = new HashSet<>();
		enrolmentsBoSet.add(enrolment);
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * Methode die de naam van het team retourneert
	 * 
	 * @return name van het team
	 */
	public String getName() {
		return name;
	}

	/**
	 * Methode voor het instellen van de teamnaam
	 * 
	 * @param name van het team
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Methode voor terugkrijgen van de Enrollments
	 * 
	 */
	public Set<EnrolmentBo> getEnrolmentsBoSet() {
		return enrolmentsBoSet;
	}
	
	/**
	 * Methode voor het zetten van de enrollments
	 * 
	 */
	public void setEnrolmentsBoHashSet(Set<EnrolmentBo> enrolmentsBoSet) {
		this.enrolmentsBoSet = enrolmentsBoSet;
	}	
}
