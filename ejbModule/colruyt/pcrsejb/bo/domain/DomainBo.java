package colruyt.pcrsejb.bo.domain;

import java.io.Serializable;

/**
 * Klasse voor het aanmaken van een domain.
 * 
 * @author jda1mbw
 */	
public class DomainBo implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;

	/**
	 * Default constructor
	 */
	public DomainBo() {
	}
	
	public DomainBo(String name) {
		setName(name);
	}

	/**
	 * Constructor voor het aanmaken van een domein met id
	 *
	 * @param id Integer
	 * @param lastName String
	 */
	public DomainBo(Integer id,String name) {
		setId(id);
		setName(name);		
	}
	

	/**
	 * Methode die de ID van een domein retourneert
	 * 
	 * @return id (Integer)
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Methode voor het instellen van een id van een domein
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Methode voor het opvragen van de naam van een domein
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Methode voor het instellen van de naam van een domein
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "domain Name=" + name;
	}
	
}
