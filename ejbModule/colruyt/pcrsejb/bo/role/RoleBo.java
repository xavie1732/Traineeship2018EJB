package colruyt.pcrsejb.bo.role;

import java.util.List;
import java.util.Objects;

import colruyt.pcrsejb.bo.competence.CraftCompetenceBo;

public class RoleBo {

	private Integer id;
	private String name;
    private List<CraftCompetenceBo> craftCompetenceBoList;

    public RoleBo() {
		super();
	}

	public RoleBo(String name, List<CraftCompetenceBo> craftCompetenceBoList) {
		super();
		this.name = name;
		this.craftCompetenceBoList = craftCompetenceBoList;
	}

	public RoleBo(Integer id, String name, List<CraftCompetenceBo> craftCompetenceBoList) {
		this.id = id;
		this.name = name;
		this.craftCompetenceBoList = craftCompetenceBoList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CraftCompetenceBo> getCraftCompetenceBoList() {
		return craftCompetenceBoList;
	}

	public void setCraftCompetenceBoList(List<CraftCompetenceBo> craftCompetenceBoList) {
		this.craftCompetenceBoList = craftCompetenceBoList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		RoleBo roleBo = (RoleBo) o;
		return Objects.equals(id, roleBo.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
