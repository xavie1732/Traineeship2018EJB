package colruyt.pcrsejb.bo.function;

import colruyt.pcrsejb.bo.competence.BehavioralCompetenceBo;
import colruyt.pcrsejb.bo.operatingunit.OperatingUnitBo;
import colruyt.pcrsejb.bo.role.RoleBo;

import java.util.HashSet;
import java.util.Set;

public class FunctionBo {

    private Integer id;
    private String title;
    private Set<RoleBo> roleBoSet = new HashSet<>();
    private Set<BehavioralCompetenceBo> behavioralCompetenceBoSet = new HashSet<>();
    private OperatingUnitBo operatingUnitBo;

    public FunctionBo() {
    }

    public FunctionBo(String title) {
        this.setTitle(title);
        this.roleBoSet = new HashSet<>();
        this.behavioralCompetenceBoSet = new HashSet<>();
    }

    public FunctionBo(String title, Set<RoleBo> roleBoSet, Set<BehavioralCompetenceBo> behavioralCompetenceBoSet) {
        super();
        this.setTitle(title);
        this.setRoleBoSet(roleBoSet);
        this.setBehavioralCompetenceBoSet(behavioralCompetenceBoSet);
    }

    public FunctionBo(Integer id, String title, Set<RoleBo> roleBoSet, Set<BehavioralCompetenceBo> behavioralCompetenceBoSet) {
        this.id = id;
        this.title = title;
        this.roleBoSet = roleBoSet;
        this.behavioralCompetenceBoSet = behavioralCompetenceBoSet;
    }

    public FunctionBo(Integer id, String title, OperatingUnitBo operatingUnitBo) {
        this.id = id;
        this.title = title;
        this.operatingUnitBo = operatingUnitBo;
    }

    public FunctionBo(Integer id, String title, Set<RoleBo> roleBoSet, Set<BehavioralCompetenceBo> behavioralCompetenceBoSet, OperatingUnitBo operatingUnitBo) {
        this.id = id;
        this.title = title;
        this.roleBoSet = roleBoSet;
        this.behavioralCompetenceBoSet = behavioralCompetenceBoSet;
        this.operatingUnitBo = operatingUnitBo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<RoleBo> getRoleBoSet() {
        return roleBoSet;
    }

    public void setRoleBoSet(Set<RoleBo> roleSet) {
        this.roleBoSet = roleSet;
    }

    public Set<BehavioralCompetenceBo> getBehavioralCompetenceBoSet() {
        return behavioralCompetenceBoSet;
    }

    public void setBehavioralCompetenceBoSet(Set<BehavioralCompetenceBo> behavioralCompetenceBoSet) {
        this.behavioralCompetenceBoSet = behavioralCompetenceBoSet;
    }

    public OperatingUnitBo getOperatingUnitBo() {
        return operatingUnitBo;
    }

    public void setOperatingUnitBo(OperatingUnitBo operatingUnitBo) {
        this.operatingUnitBo = operatingUnitBo;
    }

    @Override
    public String toString() {
        return "FunctionBo [title=" + title + ", roleSet=" + roleBoSet + ", behavioralCompetenceSet=" + behavioralCompetenceBoSet
                + "]";
    }


}
