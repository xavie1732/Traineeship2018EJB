package colruyt.pcrsejb.bo.operatingunit;

import java.util.Set;

import colruyt.pcrsejb.entity.competence.OperatingUnitCompetence;

public class OperatingUnitBo {

    private int operatingUnitId;
    private String operatingUnitName;
	private Set<OperatingUnitCompetence> operatingUnitCompetences;

    public OperatingUnitBo(Integer operatingUnitId, String operatingUnitName) {
        this.operatingUnitId = operatingUnitId;
        this.operatingUnitName = operatingUnitName;
    }

    public int getOperatingUnitId() {
        return operatingUnitId;
    }

    public void setOperatingUnitId(Integer operatingUnitId) {
        this.operatingUnitId = operatingUnitId;
    }

    public String getOperatingUnitName() {
        return operatingUnitName;
    }

    public void setOperatingUnitName(String operatingUnitName) {
        this.operatingUnitName = operatingUnitName;
    }
    
}
