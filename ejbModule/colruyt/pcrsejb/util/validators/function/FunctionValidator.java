package colruyt.pcrsejb.util.validators.function;

import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.util.exceptions.validation.ValidationException;
import colruyt.pcrsejb.util.exceptions.validation.function.InvalidFunctionTitleException;
import colruyt.pcrsejb.util.validators.GenericValidator;

public class FunctionValidator implements GenericValidator<Function> {

    @Override
    public void validate(Function toValidate) throws ValidationException {

        try {
            this.validateTitle(toValidate.getTitle());
        }
        catch(Exception e){
            //Encapsuleer de Exception
            throw new ValidationException(e);
        }
    }

    private void validateTitle(String title) throws InvalidFunctionTitleException {
        if(title == null || title.isEmpty()){
            throw new InvalidFunctionTitleException();
        }
    }

}
