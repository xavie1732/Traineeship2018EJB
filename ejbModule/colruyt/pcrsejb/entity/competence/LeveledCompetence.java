package colruyt.pcrsejb.entity.competence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SecondaryTable;

@Entity
@SecondaryTable(name="COMPETENCEDESCRIPTIONS")
public abstract class LeveledCompetence extends Competence{

	@Column(name="DESCRIPTION",table="COMPETENCEDESCRIPTIONS")
	private String description;
	@OneToMany
	@JoinColumn(name="COMPETENCE_ID")
	private List<CompetenceLevel> posibilities = new ArrayList<CompetenceLevel>();

    public LeveledCompetence() {
        super();
    }

    public LeveledCompetence(Integer id, String name, String description)
    {
    	super(id, name);
    	this.setDescription(description);
    }
    
    public LeveledCompetence(String name, String description)
    {
    	super(name);
    	this.setDescription(description);
    }

    

	public LeveledCompetence(Integer Id, String name, String description, List<CompetenceLevel> posibilities) {
		super(Id, name);
		this.description = description;
		this.posibilities = posibilities;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<CompetenceLevel> getPosibilities() {
		return posibilities;
	}

	public void setPosibilities(List<CompetenceLevel> posibilities) {
		this.posibilities = posibilities;
	}   
	
	
    
}
