package colruyt.pcrsejb.entity.competence;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="Craft") 
public class CraftCompetence extends LeveledCompetence implements RoleCompetence{

	public CraftCompetence() {
		super();
	}

	public CraftCompetence(String name, String description){
		super(name, description);
	}

	public CraftCompetence(Integer id, String name, String description) {
		super(id, name, description);
	}

	public CraftCompetence(Integer id, String name, String description, List<CompetenceLevel> competenceLevels) {
		super(id, name, description, competenceLevels);
	}
}
