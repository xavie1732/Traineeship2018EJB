package colruyt.pcrsejb.entity.competence;

import javax.persistence.*;

@Entity
@Table(name="COMPETENCES")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="COMPETENCETYPE")
@NamedQueries({
	@NamedQuery(name = "getAllCompetences", query = "select c from Competence c")
})
public abstract class Competence {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMPETENCES_SEQ")
    @SequenceGenerator(sequenceName = "COMPETENCES_SEQ", allocationSize = 1, name = "COMPETENCES_SEQ")
	@Column(name="ID")
	private Integer Id;
	@Column(name="NAME")
    private String name;

	public Competence(Integer Id, String name) {
		this.Id = Id;
		this.name = name;
	}

	public Competence() {
		super();
	}
	public Competence(String name)  {
		super();
		this.setName(name);
	}

	/**
	 * Getter for property 'Id'.
	 *
	 * @return Value for property 'Id'.
	 */
	public Integer getId() {
		return Id;
	}

	/**
	 * Setter for property 'Id'.
	 *
	 * @param Id Value to set for property 'Id'.
	 */
	public void setId(Integer Id) {
		this.Id = Id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name)  {
//		if(name == null){
//			throw new CompetenceException("name is null");
//		}
//		if(name.isEmpty()){
//			throw new CompetenceException("name is empty");
//		}
		
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Competence other = (Competence) obj;

		if (name == null) {
			if (other.name != null )
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	


}
