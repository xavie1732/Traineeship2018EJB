package colruyt.pcrsejb.entity.competence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="COMPETENCELEVELS")
public class CompetenceLevel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMPETENCELEVELS_SEQ")
    @SequenceGenerator(sequenceName = "COMPETENCELEVELS_SEQ", allocationSize = 1, name = "COMPETENCELEVELS_SEQ")
	@Column(name="ID")
	private Integer Id;
	@Column(name="ACTIVE")
	private Boolean minLevel;
	@Column(name="DESCRIPTION")
	private String description;
	@Column(name="ORDERLEVEL")
	private Integer orderLevel;
	public CompetenceLevel() {
		super();
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public Boolean getMinLevel() {
		return minLevel;
	}
	public void setMinLevel(Boolean minLevel) {
		this.minLevel = minLevel;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getOrderLevel() {
		return orderLevel;
	}
	public void setOrderLevel(Integer orderLevel) {
		this.orderLevel = orderLevel;
	} 
}
