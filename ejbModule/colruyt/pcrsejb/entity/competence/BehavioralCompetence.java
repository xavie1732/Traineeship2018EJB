package colruyt.pcrsejb.entity.competence;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="Behavioral") 
public class BehavioralCompetence extends LeveledCompetence implements FunctionCompetence {


	public BehavioralCompetence() {
		super();
	}

	public BehavioralCompetence(String name, String description){
		super(name, description);
	}

	public BehavioralCompetence(Integer id, String name, String description) {
		super(id, name, description);
	}

	public BehavioralCompetence(Integer id, String name, String description,
			List<CompetenceLevel> posibilities) {
		super(id, name, description, posibilities);
	}
}
