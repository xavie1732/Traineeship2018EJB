package colruyt.pcrsejb.entity.competence;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="OperatingUnit") 
public class OperatingUnitCompetence extends Competence {

	public OperatingUnitCompetence() {
		super();
	}

	public OperatingUnitCompetence(String name)  {
		super(name);
	}

	public OperatingUnitCompetence(Integer id, String name) {
		super(id, name);
	}
}
