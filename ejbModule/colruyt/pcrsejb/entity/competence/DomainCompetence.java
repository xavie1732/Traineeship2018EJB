package colruyt.pcrsejb.entity.competence;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="Domain") 
public class DomainCompetence extends Competence implements FunctionCompetence {

	public DomainCompetence() {
		super();
	}

	public DomainCompetence(String name)  {
		super(name);
	}

	public DomainCompetence(Integer id, String name) {
		super(id, name);
	}
}
