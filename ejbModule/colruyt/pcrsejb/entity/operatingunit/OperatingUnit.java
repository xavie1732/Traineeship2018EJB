package colruyt.pcrsejb.entity.operatingunit;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import colruyt.pcrsejb.entity.competence.OperatingUnitCompetence;

@Entity
@Table(name="OPERATINGUNITS")
public class OperatingUnit implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OPERATINGUNITS_SEQ")
    @SequenceGenerator(sequenceName = "OPERATINGUNITS_SEQ", allocationSize = 1, name = "OPERATINGUNITS_SEQ")
	@Column(name="ID")
	private Integer id;
	@Column(name="TITLE")
	private String operatingUnitName;
	@OneToMany
	@JoinColumn(name="OPERATINGUNIT_ID")
	private Set<OperatingUnitCompetence> operatingUnitCompetences;

	public OperatingUnit() {}
	
	public OperatingUnit(Integer id, String operatingUnitName) {
		setId(id);
		setOperatingUnitName(operatingUnitName);
	}
	
	public OperatingUnit(Integer id) {
		setId(id);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOperatingUnitName() {
		return operatingUnitName;
	}

	public void setOperatingUnitName(String operatingUnitName) {
		this.operatingUnitName = operatingUnitName;
	}

	public Set<OperatingUnitCompetence> getOperatingUnitCompetences() {
		return operatingUnitCompetences;
	}

	public void setOperatingUnitCompetences(Set<OperatingUnitCompetence> operatingUnitCompetences) {
		this.operatingUnitCompetences = operatingUnitCompetences;
	}
	
}
