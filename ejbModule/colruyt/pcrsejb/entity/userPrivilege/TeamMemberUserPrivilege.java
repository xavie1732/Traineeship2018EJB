package colruyt.pcrsejb.entity.userPrivilege;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import colruyt.pcrsejb.entity.function.Function;
import colruyt.pcrsejb.entity.survey.SurveySet;

@Entity
@DiscriminatorValue(value="TEAMMEMBER") 
public class TeamMemberUserPrivilege extends FunctionUserPrivilege implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(name="STARTDATEINCURRENTFUNCTION")
	private LocalDate startDateInCurrentFunction; 
	@OneToMany
	@JoinColumn(name="USERPRIVILEGE_ID")
    private Set<SurveySet> surveySetTreeSet = new TreeSet<>();
	
	public TeamMemberUserPrivilege() {};
    
	public TeamMemberUserPrivilege(PrivilegeType privilegeType, boolean active, Function function, LocalDate startDateInCurrentFunction) {
		super(privilegeType, active, function);
		setSurveySetTreeSet(new TreeSet<>());
		setStartDateInCurrentFunction(startDateInCurrentFunction);
	}
	
	public LocalDate getStartDateInCurrentFunction() {
		return startDateInCurrentFunction;
	}

	public void setStartDateInCurrentFunction(LocalDate startDateInCurrentFunction) {
		this.startDateInCurrentFunction = startDateInCurrentFunction;
	}

	public Set<SurveySet> getSurveySetTreeSet() {
		return surveySetTreeSet;
	}
	
	public void setSurveySetTreeSet(TreeSet<SurveySet> surveySetTreeSet) {
		this.surveySetTreeSet = surveySetTreeSet;
	}

	public void addSurveySet()
	{
		//TO DO: argumenten van de constructor aanvullen
		surveySetTreeSet.add(new SurveySet());
	}
}
