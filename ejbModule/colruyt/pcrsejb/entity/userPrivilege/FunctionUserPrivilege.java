package colruyt.pcrsejb.entity.userPrivilege;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import colruyt.pcrsejb.entity.function.Function;

@Entity
@DiscriminatorValue(value="FUNCTIONHOLDING") 
public class FunctionUserPrivilege extends UserPrivilege implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@ManyToOne(targetEntity=Function.class)
	private Function function;

	public FunctionUserPrivilege() {};
	
	public FunctionUserPrivilege(PrivilegeType privilegeType, boolean active, Function function) {
		super(privilegeType, active);
		setFunction(function);
	}
	
	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}
}
