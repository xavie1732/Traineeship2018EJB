package colruyt.pcrsejb.entity.userPrivilege;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import colruyt.pcrsejb.entity.function.Function;

@Entity
@DiscriminatorValue(value="FUNCTIONRESPONSIBLE") 
public class FunctionResponsibleUserPrivilege extends FunctionUserPrivilege implements Serializable {

	private static final long serialVersionUID = 1L;
	private String country;

	public FunctionResponsibleUserPrivilege() {};

	public FunctionResponsibleUserPrivilege(PrivilegeType privilegeType, boolean active, Function function, String country) {
		super(privilegeType, active, function);
		setCountry(country);
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
