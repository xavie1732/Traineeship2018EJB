package colruyt.pcrsejb.entity.userPrivilege;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="USERPRIVILEGES")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="PRIVILEGEDISCRIMINATOR")
@DiscriminatorValue(value="NON-FUNCTIONHOLDING")
public class UserPrivilege implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USERPRIVILEGES_SEQ")
    @SequenceGenerator(sequenceName = "USERPRIVILEGES_SEQ", allocationSize = 1, name = "USERPRIVILEGES_SEQ")
	@Column(name="ID")
	private Integer id;
	@NotNull
	@Enumerated(EnumType.STRING)
	private PrivilegeType privilegeType;
	@NotNull
	private boolean active;

	public UserPrivilege() {};
	
	public UserPrivilege(PrivilegeType privilegeType, boolean active) {
		setPrivilegeType(privilegeType);
		setActive(active);
	}
	
	public UserPrivilege(Integer id, PrivilegeType privilegeType, boolean active) {
		setId(id);
		setPrivilegeType(privilegeType);
		setActive(active);
	}

	public UserPrivilege(Integer id) {
		setId(id);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PrivilegeType getPrivilegeType() {
		return privilegeType;
	}

	public void setPrivilegeType(PrivilegeType privilegeType) {
		this.privilegeType = privilegeType;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
