package colruyt.pcrsejb.entity.role;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import colruyt.pcrsejb.entity.competence.CraftCompetence;
import colruyt.pcrsejb.entity.function.Function;

@Entity
@Table(name="ROLES")
public class Role implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROLES_SEQ")
    @SequenceGenerator(sequenceName = "ROLES_SEQ", allocationSize = 1, name = "ROLES_SEQ")
	@Column(name="ID")
	private Integer id;
	@Column(name="NAME")
	private String name;
	@OneToMany
	@JoinColumn(name="ROLE_ID")
    private List<CraftCompetence> craftCompetenceList;
	@ManyToOne
	private Function function;
    
    public Role() {
		super();
	}

	public Role(Integer id) {
			super();
			setId(id);
    }

	public Role(String name, List<CraftCompetence> craftCompetenceList) {
		super();
		this.name = name;
		this.craftCompetenceList = craftCompetenceList;
	}

	public Role(Integer id, String name, List<CraftCompetence> craftCompetenceList) {
		this.id = id;
		this.name = name;
		this.craftCompetenceList = craftCompetenceList;
	}

	public Role(String name, List<CraftCompetence> craftCompetenceList, Function function) {
		this.name = name;
		this.craftCompetenceList = craftCompetenceList;
		this.function = function;
	}

	public Role(Integer id, String name, List<CraftCompetence> craftCompetenceList, Function function) {
		this.id = id;
		this.name = name;
		this.craftCompetenceList = craftCompetenceList;
		this.function = function;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CraftCompetence> getCraftCompetenceList() {
		return craftCompetenceList;
	}

	public void setCraftCompetenceList(List<CraftCompetence> craftCompetenceList) {
		this.craftCompetenceList = craftCompetenceList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Role role = (Role) o;
		return Objects.equals(id, role.id);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id);
	}
}
