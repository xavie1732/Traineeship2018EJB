package colruyt.pcrsejb.entity.enrolment;

import javax.persistence.*;

import colruyt.pcrsejb.entity.role.Role;
import colruyt.pcrsejb.entity.user.User;
import colruyt.pcrsejb.entity.userPrivilege.UserPrivilege;

@Entity
@Table(name="TEAMENROLMENTS")
public class Enrolment {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLMENTS_SEQ")
    @SequenceGenerator(sequenceName = "ENROLMENTS_SEQ", allocationSize = 1, name = "ENROLMENTS_SEQ")
	@Column(name="ID")
	private Integer id;
	@ManyToOne
	private User user;
	@ManyToOne
	private UserPrivilege userPrivilege;
	private boolean active;
	@OneToOne
	private Role role;
	
	public Enrolment() {}
	public Enrolment(User user, UserPrivilege privilege, boolean active) {
		this.user = user;
		this.userPrivilege = privilege;
		this.active = active;
	}

	public Enrolment(Integer id, User user, UserPrivilege privilege, boolean active) {
		this.id = id;
		this.user = user;
		this.userPrivilege = privilege;
		this.active = active;
	}

	public Enrolment(User user, UserPrivilege userPrivilege, boolean active, Role role) {
		this.user = user;
		this.userPrivilege = userPrivilege;
		this.active = active;
		this.role = role;
	}

	public Enrolment(Integer id, User user, UserPrivilege privilege, boolean active, Role role) {
		this.id = id;
		this.user = user;
		this.userPrivilege = privilege;
		this.active = active;
		this.role = role;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UserPrivilege getPrivilege() {
		return userPrivilege;
	}
	public void setPrivilege(UserPrivilege privilege) {
		this.userPrivilege = privilege;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Getter for property 'role'.
	 *
	 * @return Value for property 'role'.
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * Setter for property 'role'.
	 *
	 * @param role Value to set for property 'role'.
	 */
	public void setRole(Role role) {
		this.role = role;
	}
}
