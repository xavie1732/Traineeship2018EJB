package colruyt.pcrsejb.entity.survey;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import colruyt.pcrsejb.entity.competence.Competence;

/**
 The type Rating. */
@Entity
@Table(name="RATINGS")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="RATINGTYPE")
@DiscriminatorValue(value="REGULAR")
public class Rating implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RATINGSS_SEQ")
    @SequenceGenerator(sequenceName = "RATINGSS_SEQ", allocationSize = 1, name = "RATINGSS_SEQ")
	@Column(name="ID")
	private Integer id;
	@Column(name="RATING_LEVEL")
    private Integer level;
    private boolean energy;
    @ManyToOne
    private Competence competence;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     Constructs a new Rating.
     */
    public Rating(){
    }
    
    
    /**
     Instantiates a new Rating.
     @param level the level
     @param energy the energy
     @param competence the competence
     */
    public Rating(Integer level, boolean energy, Competence competence){
        setLevel(level);
        setEnergy(energy);
        setCompetence(competence);
    }
    
    /**
     Getter for property 'level'.
     @return Value for property 'level'.
     */
    public Integer getLevel(){
        return level;
    }
    
    /**
     Setter for property 'level'.
     @param level Value to set for property 'level'.
     */
    public void setLevel(Integer level){
        this.level = level;
    }
    
    /**
     Getter for property 'energy'.
     @return Value for property 'energy'.
     */
    public boolean isEnergy(){
        return energy;
    }
    
    /**
     Setter for property 'energy'.
     @param energy Value to set for property 'energy'.
     */
    public void setEnergy(boolean energy){
        this.energy = energy;
    }
    
    /**
     Getter for property 'competence'.
     @return Value for property 'competence'.
     */
    public Competence getCompetence(){
        return competence;
    }
    
    /**
     Setter for property 'competence'.
     @param competence Value to set for property 'competence'.
     */
    public void setCompetence(Competence competence){
        this.competence = competence;
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o){
        if(this == o)
            return true;
        if(!(o instanceof Rating))
            return false;
        Rating rating = (Rating) o;
        return getLevel() == rating.getLevel() && isEnergy() == rating.isEnergy() && Objects.equals(getCompetence(), rating.getCompetence());
    }
    
    /** {@inheritDoc} */
    @Override
    public int hashCode(){
        return Objects.hash(getLevel(), isEnergy(), getCompetence());
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString(){
        return "Rating{" + "level=" + level + ", energy=" + energy + ", competence=" + competence + '}';
    }
}
