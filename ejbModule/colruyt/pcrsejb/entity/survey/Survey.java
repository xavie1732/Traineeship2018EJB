package colruyt.pcrsejb.entity.survey;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="SURVEYS")
public class Survey implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SURVEYS_SEQ")
    @SequenceGenerator(sequenceName = "SURVEYS_SEQ", allocationSize = 1, name = "SURVEYS_SEQ")
	@Column(name="ID")
	private Integer id;
	@Column(name="DATECOMPLETED")
    private LocalDate dateCompleted;
	@OneToMany
	@JoinColumn(name="SURVEY_ID")
    private List<Rating> ratingList;
	@Enumerated(EnumType.STRING)
    private SurveyKind surveyKind;
    
    public SurveyKind getSurveyKind() {
		return surveyKind;
	}

	public void setSurveyKind(SurveyKind surveyKind) {
		this.surveyKind = surveyKind;
	}

	public Survey() {
    }

    public Survey(LocalDate dateCompleted, List<Rating> ratingList,SurveyKind kind) {
        this.dateCompleted = dateCompleted;
        this.ratingList = ratingList;
        this.surveyKind = kind;
        
    }

    public LocalDate getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(LocalDate dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public List<Rating> getRatingList() {
        return ratingList;
    }

    public void setRatingList(List<Rating> ratingList) {
        this.ratingList = ratingList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
