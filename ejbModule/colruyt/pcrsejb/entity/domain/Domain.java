package colruyt.pcrsejb.entity.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Klasse voor het aanmaken van een domein.
 * 
 * @author jda1mbw
 */
@Entity
@Table(name="DOMAINS")
public class Domain implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOMAINS_SEQ")
    @SequenceGenerator(sequenceName = "DOMAINS_SEQ", allocationSize = 1, name = "DOMAINS_SEQ")
	@Column(name="ID")
	private Integer id;
	@Column(name="NAME")
	private String name;


	/**
	 * Default constructor
	 */
	public Domain() {
	}

	/**
	 * Constructor voor het aanmaken van een domain met id, zonder naam
	 * @param id
	 */
	public Domain(Integer id) {
		setId(id);
	}
	
	/**
	 * Constructor voor het aanmaken van een domain met id, met naam
	 * 
	 * @param id Integer
	 * @param name String
	 */
	public Domain(Integer id,String name) {
		setId(id);
		setName(name);
	}

	/**
	 * Constructor voor het aanmaken van een domain zonder id, met naam
	 * 
	 * @param name String
	 */
	public Domain(String name) {
		setName(name);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Methode voor het opvragen van de naam
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Methode voor het instellen van de naam
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Domain {" +"id=" + id + ", name='" + name;
	}

}