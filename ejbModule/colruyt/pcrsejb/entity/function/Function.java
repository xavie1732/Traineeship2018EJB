package colruyt.pcrsejb.entity.function;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import colruyt.pcrsejb.entity.competence.BehavioralCompetence;
import colruyt.pcrsejb.entity.operatingunit.OperatingUnit;
import colruyt.pcrsejb.entity.role.Role;
@Entity
@Table(name="FUNCTIONS")
@NamedQuery(name = "Function.getAllFunctions", query = "SELECT f FROM Function f")
public class Function implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FUNCTIONS_SEQ")
    @SequenceGenerator(sequenceName = "FUNCTIONS_SEQ", allocationSize = 1, name = "FUNCTIONS_SEQ")
	@Column(name="ID")
    private Integer id;
	@Column(name="TITLE")
    private String title;
	@OneToMany
	@JoinColumn(name="FUNCTIONS_ID")
    private Set<Role> roleSet = new HashSet<>();
    @OneToMany
    @JoinColumn(name="FUNCTIONS_ID")
    private Set<BehavioralCompetence> behavioralCompetenceSet = new HashSet<>();
    @ManyToOne
    private OperatingUnit operatingUnit;

    public Function() {}

    public Function(Integer id, String title) {
        setId(id);
        setTitle(title);
        this.roleSet = new HashSet<>();
        this.behavioralCompetenceSet = new HashSet<>();
    }

    public Function(String title) {
        this.setTitle(title);
        this.roleSet = new HashSet<>();
        this.behavioralCompetenceSet = new HashSet<>();
    }

    public Function(Integer id, String title, OperatingUnit operatingUnit) {
        this.id = id;
        this.title = title;
        this.operatingUnit = operatingUnit;
    }

    public Function(String title, Set<Role> roleSet, Set<BehavioralCompetence> behavioralCompetenceSet) {
        super();
        this.setTitle(title);
        this.setRoleSet(roleSet);
        this.setBehavioralCompetenceSet(behavioralCompetenceSet);
    }

    public Function(Integer id, String title, Set<Role> roleSet, Set<BehavioralCompetence> behavioralCompetenceSet) {
        this.id = id;
        this.title = title;
        this.roleSet = roleSet;
        this.behavioralCompetenceSet = behavioralCompetenceSet;
    }

    public Function(Integer id) {
        setId(id);
    }

    public Function(Integer id, String title, Set<Role> roleSet, Set<BehavioralCompetence> behavioralCompetenceSet, OperatingUnit operatingUnit) {
        this.id = id;
        this.title = title;
        this.roleSet = roleSet;
        this.behavioralCompetenceSet = behavioralCompetenceSet;
        this.operatingUnit = operatingUnit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer functionID) {
        this.id = functionID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Role> getRoleSet() {
        return roleSet;
    }

    public void setRoleSet(Set<Role> roleSet) {
        this.roleSet = roleSet;
    }

    public Set<BehavioralCompetence> getBehavioralCompetenceSet() {
        return behavioralCompetenceSet;
    }

    public void setBehavioralCompetenceSet(Set<BehavioralCompetence> behavioralCompetenceSet) {
        this.behavioralCompetenceSet = behavioralCompetenceSet;
    }
    
    public OperatingUnit getOperatingUnit() {
        return operatingUnit;
    }

    public void setOperatingUnit(OperatingUnit operatingUnit) {
        this.operatingUnit = operatingUnit;
    }

    @Override
    public String toString() {
        return "Function [title=" + title + ", roleSet=" + roleSet + ", behavioralCompetenceSet=" + behavioralCompetenceSet
                + "]";
    }

}
