package colruyt.pcrsejb.entity.team;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import colruyt.pcrsejb.entity.enrolment.Enrolment;
import colruyt.pcrsejb.entity.user.User;
import colruyt.pcrsejb.entity.userPrivilege.PrivilegeType;
import colruyt.pcrsejb.entity.userPrivilege.UserPrivilege;

/**
 * Klasse voor het aanmaken van een Team.
 * 
 * @author jda1mbw
 */
@Entity
@Table(name = "TEAMS")
//@NamedQueries({ @NamedQuery(name = "Team.getAllElements", query = "select t from Team t"),
//		@NamedQuery(name = "Team.getTeamOfUser", query = "select t from users us join userprivileges up "
//				+ "on US.ID = UP.USER_ID join teamenrolments te on UP.id = TE.USERPRIVILEGE_ID "
//				+ "join teams t on te.team_id = t.id where us.id = :id"),
//		@NamedQuery(name = "Team.getTeamOfEnrolment", query = "select t from teamenrolments te "
//				+ "join teams t on te.team_id = T.ID where te.id = :id") })
public class Team implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEAMS_SEQ")
	@SequenceGenerator(sequenceName = "TEAMS_SEQ", allocationSize = 1, name = "TEAMS_SEQ")
	@Column(name = "ID")
	private Integer id;
	private String name;
	@OneToMany
	@JoinColumn(name = "TEAM_ID")
	private Set<Enrolment> enrolments = new HashSet<>();;

	public Team() {
	}

	/**
	 * Constructor voor het aanmaken van een Team
	 * 
	 * @param name
	 * @param teamManager
	 */
	public Team(String name, User teamManager) {
		setName(name);
		UserPrivilege privilege = null;
		for (UserPrivilege priv : teamManager.getPrivileges()) {
			if (priv.getPrivilegeType() == PrivilegeType.TEAMMANAGER) {
				privilege = priv;
			}
		}
		if (privilege == null) {
			privilege = new UserPrivilege(PrivilegeType.TEAMMANAGER, true);
			Set<UserPrivilege> privileges = teamManager.getPrivileges();
			privileges.add(privilege);
			teamManager.setPrivileges(privileges);
		}
		Enrolment enrollment = new Enrolment(teamManager, privilege, true);
		enrolments.add(enrollment);
	}

	public Team(Integer id, String name, Set<Enrolment> enrolmentsSet) {
		this.id = id;
		this.name = name;
		this.enrolments = enrolmentsSet;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Methode die de naam van het team retourneert
	 * 
	 * @return name van het team
	 */
	public String getName() {
		return name;
	}

	/**
	 * Methode voor het instellen van de teamnaam
	 * 
	 * @param name
	 *            van het team
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Methode voor terugkrijgen van de Enrollments
	 *
	 */
	public Set<Enrolment> getEnrolments() {
		return enrolments;
	}

	/**
	 * Methode voor het zetten van de enrollments
	 *
	 * @param enrollments
	 */
	public void setEnrolments(Set<Enrolment> enrollments) {
		this.enrolments = enrollments;
	}

	public boolean contains(User teamMember) {
		boolean contains = false;
		for (Enrolment e : getEnrolments()) {
			if (e.getUser() == teamMember) {
				contains = true;
			}
		}
		return contains;
	}

}
