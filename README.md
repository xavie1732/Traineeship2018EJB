# Traineeship2018EJB
<server description="new server">

	<!-- Enable features -->
	<featureManager>
		<feature>javaee-8.0</feature>
		<feature>localConnector-1.0</feature>
		<feature>adminCenter-1.0</feature>
	</featureManager>

	<!-- Allow remote acces to admin section to write in this XML -->
	<remoteFileAccess>
		<writeDir>${server.config.dir}</writeDir>
	</remoteFileAccess>

	<!-- This template enables security. To get the full use of all the capabilities, 
		a keystore and user registry are required. -->

	<!-- For the keystore, default keys are generated and stored in a keystore. 
		To provide the keystore password, generate an encoded password using bin/securityUtility 
		encode and add it below in the password attribute of the keyStore element. 
		Then uncomment the keyStore element. -->

	<keyStore id="defaultKeyStore" password="Liberty" />

	<!--For a user registry configuration, configure your user registry. For 
		example, configure a basic user registry using the basicRegistry element. 
		Specify your own user name below in the name attribute of the user element. 
		For the password, generate an encoded password using bin/securityUtility 
		encode and add it in the password attribute of the user element. Then uncomment 
		the user element. -->
	<basicRegistry>
		<user name="admin" password="admin" />
	</basicRegistry>
	<administrator-role>
		<user>admin</user>
	</administrator-role>


	<!-- To access this server from a remote client add a host attribute to 
		the following element, e.g. host="*" -->
	<httpEndpoint httpPort="9080" httpsPort="9443"
		id="defaultHttpEndpoint" />

	<!-- Automatically expand WAR files and EAR files -->
	<applicationManager autoExpand="true" />
	<applicationMonitor updateTrigger="mbean" />
	<enterpriseApplication id="Traineeship2018EAR"
		location="Traineeship2018EAR.ear" name="Traineeship2018EAR">
		<classloader commonLibraryRef="OracleLib" />
	</enterpriseApplication>
	<dataSource id="OracleDS" jndiName="jdbc/OracleDS"
		type="javax.sql.XADataSource">
		<jdbcDriver libraryRef="OracleLib">
		</jdbcDriver>
	</dataSource>
	<library id="OracleLib">
		<file name="C:\dev\bin\tools\ojdbc8.jar" />
	</library>
</server>
